create database Test
go
use Test
/*用户表*/
create  table  Users						
(     
  User_Id int primary key identity (1,1), /*用户编号*/   		
  User_login_name varchar(20) NOT NULL, /*用户系统名称*/
  User_login_pwd varchar(20) NOT NULL	/*用户系统密码*/
)
/* 部门表 */
create table  Dept        
(   
  Dept_Id				int   primary key identity(1,1)    ,   /* 部门编号*/  
  Dept_Name				varchar(30)                NOT NULL, /* 部门名称 */  
  Dept_Remark           varchar(250)               NOT NULL/* 部门描述,备注 */    
)
/* 员工表 */
create table  Employee    
(    
  Employee_Id				int		primary key identity(1,1),		/* 员工编号，主键*/  
  Dept_Id                   int						 NOT NULL,	/* 所属部门编号 */  
  Employee_Name             varchar(30)              NOT NULL,  /* 姓名 */  
  Employee_Duty             varchar(20)              NOT NULL,  /* 职务 */  
  Employee_Gender           varchar(6)               NOT NULL,  /* 性别 */  
  Employee_BirthDate        datetime                 NOT NULL,  /* 出生日期 */    
  Employee_IdentityCard     varchar(20)              NOT NULL,      /* 身份证号 */  
  Employee_Address          varchar(250)             NOT NULL,      /* 住址 */  
  Employee_Phone            varchar(25)              NOT NULL,      /* 电话 */    
)
/*供应商表*/   
create  table  Supplier     
(   
  Supplier_Id			   int  primary key   identity (1,1) ,    /* 供应商编号，主键 */  
  Supplier_Name            varchar(50)              NOT NULL, /* 供应商名称 */  
  Supplier_Address         varchar(50)              NOT NULL, /* 供应商地址 */  
  Supplier_Phone           varchar(25)               NULL,     /* 供应商电话 */  
 )  
 /* 客户表*/  
create  table Customer   
(   
  Customer_Id			   int   primary key  identity(1,1) ,	/* 客户编号，主键*/  
  Customer_Name            varchar(250)              NOT NULL,	/* 客户名称 */  
  Customer_Address         varchar(250)              NOT NULL,	/* 客户地址 */    
  Customer_Phone           varchar(25)               NOT NULL,   /* 客户电话 */  
 )    
 /*商品表*/
 create  table Goods 
(
  Goods_id		int primary key identity(1,1),  /*商品编号，主键*/
  Goods_name	varchar(50) NOT NULL,			/*商品名称*/
  Goods_brand	varchar(50) NOT NULL,			/*商品品牌*/
  Goods_type	varchar(50) NOT NULL, 			/*商品类别*/
  Goods_price	money NOT NULL,					/*商品价格*/
)
/*仓库表*/
create  table Warehouse 
(
 Warehouse_Id		int primary key identity(1,1) , /*仓库编号，主键*/
 Warehouse_Name		varchar(20)  NOT NULL ,			/*仓库名称*/
 Employee_Id		varchar(20) NOT NULL,			/*负责人*/
)


--采购管理
/*采购表*/
create  table Purchase 
(
  Purchase_id		int primary key identity(1,1), /*采购编号，主键*/
  Purchase_price	money NOT NULL,				   /*采购单价*/
  Purchase_Sum		money NOT NULL,				   /*采购总额*/		
  Purchase_num		int NOT NULL, 				   /*采购数量*/
  Purchase_date		smalldatetime NOT NULL,		   /*采购日期*/
  Supplier_id		int NOT NULL,				   /*供应商编号，外键Supplier_Id*/
  Goods_id			int NOT NULL,				   /*商品编号，外键 Goods_id	*/
  Employee_Id		int NOT NULL,				   /*负责人，外键Employee_Id*/
 )
/* 入库单表 */ 
 create  table InStock    
(   
  InStock_Id		int primary key identity(1,1),    /* 入库单编号 */  
  InStock_Date      datetime                NOT NULL, /* 入库时间 */  
  Warehouse_Id		int                     NOT NULL, /* 所入仓库 ,外键 Warehouse_Id*/  
  Good_Id			int						NOT NULL, /*商品编号，外键 Goods_id	*/
  Employee_Id		varchar(20)				NOT NULL, /*负责人,外键Employee_Id*/

)
/*采购退货单表*/
create table Purchase_Returns
(
  Return_id		int primary key identity(1,1),  /*退货编号*/
  Return_sum	money			NOT NULL,	    /*退货总额*/
  Employee_Id	varchar(20)		NOT NULL,		/*负责人，外键Employee_Id*/
  Return_date	timestamp		NOT NULL ,		/*退货时间*/
  Goods_id		int				NOT NULL,       /*商品编号，外键 Goods_id	*/
  Purchase__id	int				NOT NULL,	    /*采购编号，外键Purchase_id*/
 )


--销售管理
/*销售表*/
create table Sales 
(
  Sales_Id			int			primary key identity(1,1),	/*销售编号，主键*/
  Sales_Price		money		NOT NULL,					/*销售单价*/
  Sales_Num			int			NOT NULL,      				/*销售数量*/
  Sales_Sum			money		NOT NULL,					/*销售总额*/	
  Sales_Date		datetime	NOT NULL,					/*销售日期*/
  Goods_Id			int			NOT NULL,					/*商品编号，外键 Goods_id	*/
  Customer_Id	    int			NOT NULL,					/*客户编号，外键Customer_Id	*/	
  Employee_Id       varchar(20) NOT NULL,					/*负责人，外键Employee_Id*/
)
/* 出库单表 */  
create  table OutStock    
(   
  OutStock_Id			int primary key identity(1,1),		/* 出库单编号 */  
  OutStock_Date         datetime                NOT NULL,	/* 出库时间 */  
  Warehouse_Id			int                     NOT NULL,	/* 所出仓库 ,外键 Warehouse_Id*/  
  Good_Id				int						NOT NULL,   /*商品编号,外键 Goods_id	*/
  Employee_Id			varchar(20)				NOT NULL,	/*负责人，外键Employee_Id*/
)
/*销售退货单表*/
create table Sales_Returns
(
  Sales_Return_id		int primary key identity(1,1),  /*退货编号*/
  Sales_Return_sum		money			NOT NULL,	    /*退货总额*/
  Employee_Id			varchar(20)		NOT NULL,		/*负责人，外键Employee_Id*/
  Sales_Return_date		timestamp		NOT NULL ,		/*退货时间*/
  Goods_id				int				NOT NULL,       /*商品编号，外键 Goods_id	*/
  Sales_id				int				NOT NULL,	    /*销售编号，外键Sales_Id*/
 )


--库存管理
/* 库存表 */  
create table StockPile  
(    
  StockPile_Id			int primary key  identity(1,1)   ,  /* 库存编号 */  
  Warehouse_Id			int                      NOT NULL,	/* 所在仓库,外键 Warehouse_Id*/      
  Goods_Id				int                      NOT NULL,	/* 商品编号,外键 Goods_id	 */      
  FirstEnterDate		datetime				 NOT NULL,	/* 此种商品第一次入库时间 */  
  LastLeaveDate			datetime				 NOT NULL,  /* 此种商品最后一次出库时间 */  
  Quantity				int                      NOT NULL,  /* 库存数量 */  
  StockPile_Address     varchar(250)		     NOT NULL,	/* 仓库地址 */    
)

